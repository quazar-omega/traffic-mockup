# Traffic Mockup

Mockups of application idea "Traffic" meant to track the data usage of the PC and check the related statistics.

## Dashboard

![Dashboard tab](./exports/dashboard.png)

## Apps

![Apps tab](./exports/apps.png)

---
style: GNOME (GTK4 + libadwaita)

# Contribute
You can clone this repository and edit the svg file, then open a pull request.